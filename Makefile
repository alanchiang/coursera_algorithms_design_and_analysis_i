COMPILER_FLAGS = -c -g -O2 -Wall -Werror

lib.o: lib.h lib.cpp
	g++ $(COMPILER_FLAGS) lib.cpp

clean:
	rm -f *.o
