#include "../lib.h"

#include <iostream>
#include <string>
#include <vector>

using namespace lib;
using std::string;
using std::cout;
using std::endl;
using std::vector;

unsigned int num_inversions(vector<int> &int_list);
unsigned int num_split_inv(vector<int> &left, vector<int> &right, vector<int> &merged);

int main(int argc, char* argv[]) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}

	string input(argv[1]);
	vector<int> int_list = vector_from_file(input);
	unsigned int num_inv = num_inversions(int_list);
	cout<<"There are "<<num_inv<<" inversions."<<endl;
	return 0;
}

unsigned int num_inversions(vector<int> &int_list) {
	if (int_list.size() <= 1)
		return 0;

	vector<int> left(int_list.begin(), int_list.begin() + int_list.size()/2);
	vector<int> right(int_list.begin() + int_list.size()/2, int_list.end());
	vector<int> merged;

	unsigned int left_inv = num_inversions(left);
	unsigned int right_inv = num_inversions(right);
	unsigned int split_inv = num_split_inv(left, right, merged);

	int_list = merged;

	return left_inv + right_inv + split_inv;
}

unsigned int num_split_inv(vector<int> &left, vector<int> &right, vector<int> &merged) {
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int num_inv = 0;

	while (i < left.size() || j < right.size()) {
		if (i < left.size() && j < right.size()) {
			if (left[i] < right[j])
				merged.push_back(left[i++]);
			else {
				merged.push_back(right[j++]);
				num_inv += left.size() - i;
			}
		} else {
			if (i >= left.size() && j < right.size()) {
				while (j < right.size())
					merged.push_back(right[j++]);
			} else {
				while (i < left.size())
					merged.push_back(left[i++]);
			}
		}
	}
	return num_inv;
}
