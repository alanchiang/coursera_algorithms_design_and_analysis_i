#include "../lib.h"

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

using namespace lib;
using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::swap;
using std::map;
using std::sort;
using boost::lexical_cast;

unsigned int num_quicksort_comp(vector<int> &int_list, int left, int right, int sorting_option);
int choose_pivot(const vector<int> &int_list, int left, int right, int sorting_option);
int median_of_three(const vector<int> &int_list, int left, int right);

int main(int argc, char **argv) {
	if (argc != 3) {
		cout<<"Argument must be single text file and a sorting option."<<endl;
		return 1;
	}

	string input(argv[1]);
	string sorting_option(argv[2]);
	vector<int> int_list = vector_from_file(input);
	unsigned int num_comp = num_quicksort_comp(int_list, 0, int_list.size(),lexical_cast<int>(sorting_option));
	cout<<"There are "<<num_comp<<" comparisons."<<endl;
	return 0;
}

unsigned int num_quicksort_comp(vector<int> &int_list, int left, int right, int sorting_option) {
	if (right - left <= 0)
		return 0;

	unsigned int num_comp = right - left - 1;

	int pivot_index = choose_pivot(int_list, left, right, sorting_option);
	swap(int_list[left], int_list[pivot_index]);
	int new_left = left;
	for (int i = left + 1; i < right; i++) {
		if (int_list[i] < int_list[left])
			swap(int_list[++new_left], int_list[i]);
	}
	swap(int_list[new_left], int_list[left]);
	unsigned int left_comp = num_quicksort_comp(int_list, left, new_left, sorting_option);
	unsigned int right_comp = num_quicksort_comp(int_list, new_left + 1, right, sorting_option);
	return num_comp + left_comp + right_comp;
}

int choose_pivot(const vector<int> &int_list, int left, int right, int sorting_option) {
	if (sorting_option == 0)
		return left;
	else if (sorting_option == 1)
		return right - 1;
	else 
		return median_of_three(int_list, left, right);
}

int median_of_three(const vector<int> &int_list, int left, int right) {
	int mid = left + (right - 1 -left)/2;
	map< int, int > indices;
	indices[int_list[left]] = left;
	indices[int_list[right - 1]] = right - 1;
	indices[int_list[mid]] = mid;
	vector<int> sorted;
	sorted.push_back(int_list[left]);
	sorted.push_back(int_list[right - 1]);
	sorted.push_back(int_list[mid]);
	sort(sorted.begin(), sorted.end());
	return indices[sorted[1]];
}
