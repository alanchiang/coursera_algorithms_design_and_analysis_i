#include "../lib.h"

#include <iostream>
#include <string>
#include <map>
#include <list>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace lib;

double random_fraction();
int guess_min_cut(map< int,list<int> > adj_list);
void merge_nodes(map< int, list<int> > &adj_list, map< int, list<int> >::iterator &it, list<int>::iterator &jt);

int main(int argc, char** argv) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}
	
	string input(argv[1]);
	map< int, list<int> > adj_list = adj_list_from_file(input);
	int num_trials = 100;
	srand(time(NULL));
	int min_cut_estimate = guess_min_cut(adj_list);
	for (int i = 0; i < num_trials; i++) 
		min_cut_estimate = min(guess_min_cut(adj_list), min_cut_estimate);
	
	cout<<"The number of edges in a min cut might be "<<min_cut_estimate<<"."<<endl;
	return 0;
}

double random_fraction() {
	return (rand() % 100)/100.0;
}

int guess_min_cut(map< int,list<int> > adj_list) {
	while (adj_list.size() > 2) {
		int steps = adj_list.size() * random_fraction();
		map< int, list<int> >::iterator it = adj_list.begin();
		for (int i = 0; i < steps; i++)
			it++;

		steps = it->second.size() * random_fraction();
		list<int>::iterator jt = it->second.begin();
		for (int i = 0; i < steps; i++)
			jt++;

		merge_nodes(adj_list, it, jt);
	}

	return adj_list.begin()->second.size();
}

void merge_nodes(map< int, list<int> > &adj_list, map< int, list<int> >::iterator &it, list<int>::iterator &jt) {
	int first_node = it->first;
	int second_node = *jt;

	for (list<int>::iterator i = adj_list[second_node].begin(); i != adj_list[second_node].end(); i++) {
		if (*i != first_node) {
			adj_list[first_node].push_back(*i);
			for (list<int>::iterator j = adj_list[*i].begin(); j != adj_list[*i].end(); j++) {
				if (*j == second_node) 
					*j = first_node;
			}
		}
	}

	adj_list[first_node].remove(second_node);
	adj_list.erase(second_node);
}
