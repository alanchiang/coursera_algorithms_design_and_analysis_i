#include "../lib.h"

#include <iostream>
#include <string>
#include <map>
#include <list>
#include <algorithm>
#include <stack>

using namespace lib;
using namespace std;

map<int, list<int> > tarjan(map<int, list<int> > &del);
void tarjan_helper(int vertex, int &index, map<int, int> &indices, map<int, int> &lowlink, stack<int> &vertices, map<int, bool> &smap, map<int, list<int> > &del, map<int, list<int> > &retval);
	
int main(int argc, char** argv) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}

	string input(argv[1]);
	map<int, list<int> > del = adj_list_from_file(input);
	map<int, list<int> > scc = tarjan(del);

	vector<int> sizes;
	for (map<int, list<int> >::iterator i = scc.begin(); i != scc.end(); i++) {
		sizes.push_back(i->second.size());
	} 

	sort(sizes.rbegin(), sizes.rend());
	int nth_largest = 5;
	string formatted;
	for (vector<int>::iterator i = sizes.begin(); i != sizes.end() && nth_largest > 0; i++) {
		formatted += int_to_string(*i) + ",";
		nth_largest--;
	}
	formatted.erase(formatted.size() - 1);
	cout<<formatted<<endl;

	return 0;
}

map<int, list<int> > tarjan(map<int, list<int> > &del) {
	int index = 0;
	stack<int> vertices;
	map<int, int> indices;
	map<int, int> lowlink;
	map<int, list<int> > retval;
	map<int, bool> smap;

	for (map<int, list<int> >::iterator i = del.begin(); i != del.end(); i++) {
		if (!indices.count(i->first)) {
			tarjan_helper(i->first, index, indices, lowlink, vertices, smap, del, retval);
		}
	}
	return retval;
}

void tarjan_helper(int vertex, int &index, map<int, int> &indices, map<int, int> &lowlink, stack<int> &vertices, map<int, bool> &smap, map<int, list<int> > &del, map<int, list<int> > &retval) {
	indices[vertex] = index;
	lowlink[vertex] = index;
	index++;
	vertices.push(vertex);
	smap[vertex] = true;
	for (list<int>::iterator i = del[vertex].begin(); i != del[vertex].end(); i++) {
		if (!indices.count(*i)) {
			tarjan_helper(*i, index, indices, lowlink, vertices, smap, del, retval);
			lowlink[vertex] = min(lowlink[vertex], lowlink[*i]);
		} else if (smap.count(*i)) {
			lowlink[vertex] = min(lowlink[vertex], lowlink[*i]);
		}
	}

	if (lowlink[vertex] == indices[vertex]) {
		while (!vertices.empty() && vertices.top() != vertex) {
			retval[vertex].push_back(vertices.top());
			smap.erase(vertices.top());
			vertices.pop();
		}
		if (!vertices.empty() && vertices.top() == vertex) {
			retval[vertex].push_back(vertices.top());
			smap.erase(vertices.top());
			vertices.pop();
		}
	}
}
