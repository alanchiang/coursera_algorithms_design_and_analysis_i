//#define _TESTING_
#include "../lib.h"

#include <string>
#include <map>
#include <list>
#include <utility>
#include <queue>
#include <iostream>

using namespace std;
using namespace lib;

struct Vertex {
	int id;
	unsigned int tent_dist;
	Vertex(int vertex, unsigned int dist) : id(vertex), tent_dist(dist) {}
};

class VertexCompare {
	public:
	bool operator()(const Vertex &a, const Vertex &b) {
		return a.tent_dist > b.tent_dist;
	}
};

map<int, unsigned int> dijkstra(map<int, list< pair<int, unsigned int> > > &weighted_adj_list);

int main(int argc, char** argv) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}

	string input(argv[1]);
	map<int, list< pair<int, unsigned int> > > weighted_adj_list = weighted_adj_list_from_file(input);
	map<int, unsigned int> distances = dijkstra(weighted_adj_list);

	#ifdef _TESTING_
	for (map<int, unsigned int>::iterator i = distances.begin(); i != distances.end(); i++) {
		cout<<"node: "<<i->first<<" cost: "<<i->second<<endl;
	}
	#else
	string formatted;
	int vert[] = {7,37,59,82,99,115,133,165,188,197};
	vector<int> vertices(vert, vert + (sizeof vert)/sizeof(int));
	for (vector<int>::iterator i = vertices.begin(); i != vertices.end(); i++)
		formatted += int_to_string(distances[*i]) + ",";

	formatted.erase(formatted.size() - 1);
	cout<<formatted<<endl;
	#endif

	return 0;
}

map<int, unsigned int> dijkstra(map<int, list< pair<int, unsigned int> > > &weighted_adj_list) {
	Vertex  source_vertex(1,0);
	unsigned int default_dist = 1000000;
	map<int, unsigned int> distances;
	map<int, bool> visited;
	priority_queue<Vertex, vector<Vertex>, VertexCompare> to_be_visited;

	for (map<int, list< pair<int, unsigned int> > >::iterator i = weighted_adj_list.begin(); 
		i != weighted_adj_list.end(); i++) {
		distances[i->first] = default_dist;
	}

	distances[source_vertex.id] = 0;
	to_be_visited.push(source_vertex);

	while (!to_be_visited.empty()) {
		Vertex cur_vertex = to_be_visited.top();
		unsigned int cur_dist = distances[cur_vertex.id];
		to_be_visited.pop();
		if (visited.count(cur_vertex.id)) {
			continue;
		}

		for (list< pair<int, unsigned int> >::iterator i = weighted_adj_list[cur_vertex.id].begin(); 
			i != weighted_adj_list[cur_vertex.id].end(); i++) {
			if (!visited.count(i->first)) {
				if (cur_dist + i->second < distances[i->first]) {
					distances[i->first] = cur_dist + i->second;
				}
				Vertex push_me(i->first, distances[i->first]);
				to_be_visited.push(push_me);
			}
		}
		visited[cur_vertex.id] = true;
	}

	return distances;
}
