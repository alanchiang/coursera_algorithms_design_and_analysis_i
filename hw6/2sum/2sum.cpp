#include "../../lib.h"

#include <string>
#include <iostream>
#include <map>

using namespace std;
using namespace lib;

int two_sum_variant(map<double, int> &integers);
double floor_binary_search(vector<double> &int_list, double target);

int main(int argc, char** argv) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}

	string input(argv[1]);
	map<double, int> integers = map_from_file(input);
	int num_distinct_sums = two_sum_variant(integers);
	cout<<num_distinct_sums<<endl;

	return 0;
}

int two_sum_variant(map<double, int> &integers) {
	double lower_target = -10000;
	double  higher_target = 10000;
	map<double, bool> distinct_sums;
	vector<double> int_list;

	for (map<double, int>::iterator i = integers.begin(); i != integers.end(); i++) 
		int_list.push_back(i->first);

	for (map<double, int>::const_iterator i = integers.begin(); i != integers.end(); i++) {
		double low_bound = floor_binary_search(int_list, lower_target - i->first);
		double hi_bound = higher_target - i->first;

		map<double, int>::const_iterator j = integers.find(low_bound);
		while (j != integers.end() && j->first <= hi_bound) {
			double num = i->first;
			double comp = j->first;
			if (num != comp && num + comp >= lower_target && num + comp <= higher_target) {
				distinct_sums[num + comp] = true;
			}
			j++;
		}
	}

	return distinct_sums.size();
}

double floor_binary_search(vector<double> &int_list, double target) {
	int lower = 0;
	int upper = int_list.size() - 1;
	int mid = lower + (upper - lower)/2;
	while (upper > lower) {
		mid = lower + (upper - lower)/2;
		if (int_list[mid] == target)
			return target;
		else if (int_list[mid] > target) {
			upper = mid - 1;
		} else {
			lower = mid + 1;
		}
	}

	while (int_list[lower] > target)
		lower--;

	return int_list[lower];
}
