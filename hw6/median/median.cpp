#include "../../lib.h"

#include <string>
#include <iostream>
#include <vector>
#include <queue>
#include <functional>

using namespace std;
using namespace lib;

int sum_medians(vector<int> int_list);

class Median {
	public:
		void push(int i);
		int cur_median();

	private:
		priority_queue<int, vector<int>, greater<int> > higher;
		priority_queue<int, vector<int>, less<int> > lower;
};

void Median::push(int i) {
	lower.push(i);
	while (lower.size() - higher.size() > 1) {
		higher.push(lower.top());
		lower.pop();
	}

	while (!higher.empty() && higher.top() < lower.top()) {
		int swap = lower.top();
		lower.pop();
		lower.push(higher.top());
		higher.pop();
		higher.push(swap);
	}
}

int Median::cur_median() {
	return lower.top();
}

int main(int argc, char** argv) {
	if (argc != 2) {
		cout<<"Argument must be single text file."<<endl;
		return 1;
	}

	string input(argv[1]);
	vector<int> int_list = vector_from_file(input);
	int sum = sum_medians(int_list);
	cout<<sum % 10000<<endl;	

	return 0;
}


int sum_medians(vector<int> int_list) {
	Median medians;
	int sum = 0;
	for (vector<int>::iterator i = int_list.begin(); i != int_list.end(); i++) {
		medians.push(*i);
		sum += medians.cur_median();
	}
	return sum;
}
