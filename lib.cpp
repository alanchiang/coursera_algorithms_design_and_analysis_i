#include "lib.h"

int lib::string_to_int(const std::string &convert_me) {
	int retval;
	std::stringstream convert(convert_me);
	convert>>retval;
	return retval;
}

double lib::string_to_double(const std::string &convert_me) {
	double retval;
	std::stringstream convert(convert_me);
	convert>>retval;
	return retval;
}

std::string lib::int_to_string(int convert_me) {
	std::string retval;
	do {
		retval.insert(0, 1, char((convert_me % 10)+48));
		convert_me /= 10;
	} while(convert_me > 0); 
	return retval;
}

std::vector<int> lib::vector_from_file(const std::string input) {
	std::ifstream input_file(input.c_str());

	if (input_file.is_open()) {
		std::vector<int> int_list;
		std::string line;

		while (getline(input_file, line).good())
			int_list.push_back(string_to_int(line));

		input_file.close();
		return int_list;
	} else { throw lib::Error("File could not be open"); }
}

std::map<double, int> lib::map_from_file(const std::string input) {
	std::ifstream input_file(input.c_str());

	if (input_file.is_open()) {
		std::map<double, int> count;
		std::string line;

		while (getline(input_file, line).good()) 
			count[string_to_double(line)]++;

		input_file.close();
		return count;
	} else { throw lib::Error("File could not be open"); }
}

std::list<std::string> lib::tokenize(const std::string line) {
	std::list<std::string> retval;
	std::string new_entry;
	for (unsigned int i = 0; i < line.size() + 1; i++) {
		if (i != line.size() && !isspace(line[i]) && !ispunct(line[i])) {
			new_entry.append(1, line[i]);
		} else {
			if (!new_entry.empty()) {
				retval.push_back(new_entry);
				new_entry.clear();
			}
		}
	}
	return retval;
}

std::map< int, std::list<int> > lib::adj_list_from_file(const std::string input) {
	std::ifstream input_file(input.c_str());

	if (input_file.is_open()) {
		std::map< int, std::list<int> > adj_list;
		std::string line;

		while (getline(input_file, line).good()) {
			std::list<std::string> tokens = tokenize(line);
			std::list<std::string>::iterator i = tokens.begin();
			for (i++; i != tokens.end(); i++) {
				adj_list[string_to_int(*tokens.begin())].
					push_back(string_to_int(*i));
			}
		}

		input_file.close();
		return adj_list;
	} else { throw lib::Error("File could not be open"); }
}

std::map< int, std::list< std::pair<int, unsigned int> > > lib::weighted_adj_list_from_file(const std::string input) {
	std::ifstream input_file(input.c_str());

	if (input_file.is_open()) {
		std::map< int, std::list< std::pair<int, unsigned int> > > weighted_adj_list;
		std::string line;

		while (getline(input_file, line).good()) {
			std::list<std::string> tokens = tokenize(line);
			std::list<std::string>::iterator i = tokens.begin();
			weighted_adj_list[string_to_int(*i)].empty();
			for (i++; i != tokens.end(); i++) {
				int vertex = string_to_int(*i++);
				int weight = string_to_int(*i);
				std::pair<int, unsigned int> new_pair(vertex, weight);
				weighted_adj_list[string_to_int(*tokens.begin())].
					push_back(new_pair);
			}
		}

		input_file.close();
		return weighted_adj_list;
	} else { throw lib::Error("File could not be open"); }
}
