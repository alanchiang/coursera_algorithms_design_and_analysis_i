#ifndef LIB_H
#define LIB_H
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <iostream>
#include <map>
#include <list>
#include <cctype>
#include <utility>

namespace lib {
	class Error {
		public :
			std::string message;
			Error(std::string msg) {
				message = msg;
				std::cout<<message<<std::endl;
			}
	};
	int string_to_int(const std::string &convert_me);
	double string_to_double(const std::string &convert_me);
	std::string int_to_string(int convert_me);
	std::vector<int> vector_from_file(const std::string input);
	std::map<double, int> map_from_file(const std::string input);
	std::list<std::string> tokenize(const std::string line);
	std::map< int, std::list<int> > adj_list_from_file(const std::string input);
	std::map< int, std::list< std::pair<int, unsigned int> > > weighted_adj_list_from_file(const std::string input);

}

#endif
